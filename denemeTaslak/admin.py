from django.contrib import admin

from denemeTaslak.models import Question, Choice

# Register your models here.

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_title','question_text', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['question_text']

class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('id','question','choice','vote')
    #list_filter = ['pub_date']
    search_fields = ['choice']

admin.site.register(Question, QuestionAdmin)

admin.site.register(Choice,ChoiceAdmin)