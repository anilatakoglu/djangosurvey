from django import forms
from denemeTaslak.models import Question
from denemeTaslak.models import Choice

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['question_text',
                  'question_title']


class ChoiceForm(forms.ModelForm):
    class Meta:
        model = Choice
        fields = ['choice']
