from django.core.urlresolvers import reverse, reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.views import generic
from denemeTaslak.forms import QuestionForm
from denemeTaslak.forms import ChoiceForm
from denemeTaslak.models import Question
from denemeTaslak.models import Choice

from django.http import HttpResponse, HttpResponseRedirect


# Create your views here.


class IndexView(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'lastest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-pub_date')


class DetailView(generic.DetailView, generic.View):
    model = Question
    template_name = 'detail.html'



    def post(self, request, pk):
        if request.POST.get('choice'):
            choice = Choice.objects.get(pk=request.POST.get('choice'))
            choice.vote += 1
            choice.save()
            return HttpResponseRedirect('/question/%s/results/' % self.kwargs.get('pk'))
        else:
            return HttpResponseRedirect('/question/%s/' % self.kwargs.get('pk'))

class QuestionView(generic.View):
    # model = QuestionForm
    def get(self, request):
        question_form = QuestionForm()
        return render(request, 'addQuestion.html', {
            'question_form': question_form
        })

    def post(self, request):

        question_form = QuestionForm(request.POST)
        if question_form.is_valid():
            question_form.save()
            return HttpResponseRedirect(reverse('index'))

        else:

            return render(request, 'addQuestion.html', {
                'question_form': question_form,
                'errors': question_form.errors

            })

    template_name = "addQuestion.html"
    form_class = QuestionForm
    success_url = '/'


class ChoiceView(generic.View):
    def get(self, request, pk):
        choice_form = ChoiceForm()
        return render(request, 'add_choice.html', {
            'choice_form': choice_form
        })

    def post(self, request, pk):

        choice_form = ChoiceForm(request.POST)

        if choice_form.is_valid():
            Choice.objects.create(question_id=int(pk), choice=choice_form.data.get('choice'))

            return HttpResponseRedirect('/question/%s/' % self.kwargs.get('pk'))

        else:

            return render(request, 'add_choice.html', {
                'choice_form': choice_form,
                'errors': choice_form.errors

            })

    template_name = "add_choice.html"
    form_class = ChoiceForm
    success_url = '/'
    fields = 'question_id'


class DeleteChoice(generic.DeleteView):
    template_name = 'delete_choice.html'

    model = Choice

    def get(self, request,  *args, **kwargs):
        self.object = self.get_object()
        question_id = self.object.question_id
        self.object.delete()
        #Choice.objects.get(self.object.id).delete()
        return HttpResponseRedirect('/question/%s/' % question_id)

class DeleteQuestion(generic.DeleteView):
    template_name = 'delete_question.html'
    model = Question

    def get(self, request,  *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        #Choice.objects.get(self.object.id).delete()
        return HttpResponseRedirect(reverse('index'))


class VoteView(generic.DetailView):
    template_name = "results.html"
    model = Question

