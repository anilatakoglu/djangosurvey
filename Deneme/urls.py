"""Deneme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from denemeTaslak import views

urlpatterns = [


    #url(r'^detail/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^$',views.IndexView.as_view(), name='index'),
    url(r'^admin/', admin.site.urls),
    url(r'^question/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^add_question/$', views.QuestionView.as_view(), name='add_question'),
    url(r'^question/(?P<pk>[0-9]+)/add_choice/$', views.ChoiceView.as_view(), name='add_choice'),
    url(r'^question/(?P<pk>[0-9]+)/results/$', views.VoteView.as_view(), name='results'),
    url(r'^delete_choice/(?P<pk>[0-9]+)/$', views.DeleteChoice.as_view(), name='delete_choice'),
    url(r'^delete_question/(?P<pk>[0-9]+)/$', views.DeleteQuestion.as_view(), name='delete_question'),
]


