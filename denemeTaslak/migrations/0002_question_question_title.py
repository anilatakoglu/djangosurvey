# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-13 10:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('denemeTaslak', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='question_title',
            field=models.CharField(default=0, max_length=100),
        ),
    ]
