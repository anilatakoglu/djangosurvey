from __future__ import unicode_literals

from django.db import models


# Create your models here.

class Question(models.Model):
    question_title = models.CharField(max_length=100)
    question_text = models.CharField(max_length=300)
    pub_date = models.DateTimeField('date published', auto_now_add=True)

    def __str__(self):
        return self.question_text

    def __str__(self):
        return self.question_title


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice = models.CharField(max_length=300)
    vote = models.IntegerField(default=0)

    def __str__(self):
        return self.choice

